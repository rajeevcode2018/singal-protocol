use std::os::raw as types;

use axolotl;
use curve;
use protocol;
use ratchet;

// Session types
pub enum PreKey          {}
pub enum SignedPreKey    {}
pub enum PreKeyBundle    {}
pub enum Builder         {}
pub enum Record          {}
pub enum RecordStateNode {}
pub enum State           {}
pub enum Cipher          {}


// session_builder.h //
/*
 * Session builder is responsible for setting up encrypted sessions.
 * Once a session has been established, a Cipher
 * can be used to encrypt/decrypt messages in that session.
 *
 * Sessions are built from one of three different possible vectors:
 * - A PreKeyBundle retrieved from a server
 * - A pre_key_signal_message received from a client
 * - A KeyExchangeMessage sent to or received from a client
 *
 * Sessions are constructed per AXOLOTL address
 * (recipient name + device ID tuple). Remote logical users are identified by
 * their recipient name, and each logical recipient can have multiple
 * physical devices.
 */
extern "C" {
	/**
	 * Constructs a session builder.
	 *
	 * The store and global contexts must remain valid for the lifetime of the
	 * session builder.
	 *
	 * When finished, free the returned instance by calling session_builder_free().
	 *
	 * @param builder set to a freshly allocated session builder instance
	 * @param store the ::StoreContext to store all state information in
	 * @param remote_address the address of the remote user to build a session with
	 * @param global_context the global library context
	 * @return 0 on success, or negative on failure
	 */
	pub fn session_builder_create(
			builder: *mut *mut Builder,
			store: *mut ::StoreContext,
			remote_address: *const axolotl::Address,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Build a new session from a received pre_key_signal_message.
	 *
	 * After a session is constructed in this way, the embedded SignalMessage
	 * can be decrypted.
	 *
	 * @param message The received PreKeySignalMessage.
	 * @param unsigned_pre_key_id set to the unsigned pre key ID, if available.
	 *     Return value indicates whether or not this value is available.
	 * @retval 0 Success, no unsigned pre key value available
	 * @retval 1 Success, an unsigned pre key is available
	 * @retval AX_ERR_INVALID_KEY_ID when there is no local pre_key_record that
	 *                               corresponds to the PreKey ID in the message.
	 * @retval AX_ERR_INVALID_KEY when the message is formatted incorrectly.
	 * @retval AX_ERR_UNTRUSTED_IDENTITY when the identity key of the sender is untrusted.
	 */
	pub fn session_builder_process_pre_key_signal_message(
			builder: *mut Builder,
			record:  *mut Record,
			message: *mut protocol::PreKeySignalMessage,
			unsigned_pre_key_id: *mut u32
		) -> ::OkStatus;
	
	/**
	 * Build a new session from a PreKeyBundle retrieved from a server.
	 *
	 * @param bundle A pre key bundle for the destination recipient, retrieved from a server.
	 * @retval AX_SUCCESS Success
	 * @retval AX_ERR_INVALID_KEY when the PreKeyBundle is badly formatted.
	 * @retval AX_ERR_UNTRUSTED_IDENTITY when the sender's identity key is not trusted.
	 */
	pub fn session_builder_process_pre_key_bundle(
			builder: *mut Builder,
			bundle:  *mut PreKeyBundle
		) -> ::OkStatus;
	
	/**
	 * Build a new session from a KeyExchangeMessage received from a remote client.
	 *
	 * @param message The received KeyExchangeMessage.
	 * @param response_message Set to the KeyExchangeMessage to respond with,
	 *     or set to 0 if no response is necessary.
	 * @retval AX_SUCCESS Success
	 * @retval AX_ERR_INVALID_KEY if the received KeyExchangeMessage is badly formatted.
	 * @retval AX_ERR_UNTRUSTED_IDENTITY
	 * @retval AX_ERR_STALE_KEY_EXCHANGE
	 */
	pub fn session_builder_process_key_exchange_message(
			builder: *mut Builder,
			message: *mut protocol::KeyExchangeMessage,
			response_message: *mut *mut protocol::KeyExchangeMessage
		) -> ::OkStatus;
	
	/**
	 * Initiate a new session by sending an initial KeyExchangeMessage to the recipient.
	 *
	 * @param message Set to the KeyExchangeMessage to deliver.
	 * @return AX_SUCCESS on success, negative on error
	 */
	pub fn session_builder_process(
			builder: *mut Builder,
			message: *mut *mut protocol::KeyExchangeMessage
		) -> ::OkStatus;
	
	pub fn session_builder_free(
			builder: *mut Builder
		);
}


// session_cipher.h //
/*
 * The main entry point for Axolotl encrypt/decrypt operations.
 *
 * Once a session has been established with the session Builder,
 * this class can be used for all encrypt/decrypt operations within
 * that session.
 */
extern "C" {
	/**
	 * Construct a session cipher for encrypt/decrypt operations on a session.
	 * In order to use session_cipher, a session must have already been created
	 * and stored using session_builder.
	 *
	 * The store and global contexts must remain valid for the lifetime of the
	 * session cipher.
	 *
	 * When finished, free the returned instance by calling session_cipher_free().
	 *
	 * @param cipher set to a freshly allocated session cipher instance
	 * @param store the axolotl_store_context to store all state information in
	 * @param remote_address the remote address that messages will be encrypted to or decrypted from.
	 * @param global_context the global library context
	 * @return 0 on success, or negative on failure
	 */
	pub fn session_cipher_create(
			cipher: *mut *mut Cipher,
			store:  *mut ::StoreContext,
			remote_address: *const axolotl::Address,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	/**
	 * Set the optional user data pointer for the session cipher.
	 *
	 * This is to give callback functions a way of accessing app specific
	 * context information for this cipher.
	 */
	pub fn session_cipher_set_user_data(
			cipher:    *mut Cipher,
			user_data: *mut types::c_void
		);
	
	/**
	 * Get the optional user data pointer for the session cipher.
	 *
	 * This is to give callback functions a way of accessing app specific
	 * context information for this cipher.
	 */
	pub fn session_cipher_get_user_data(
			cipher: *mut Cipher
		) -> *mut types::c_void;
	
	/**
	 * Set the callback function that is called during the decrypt process.
	 *
	 * The callback function is called from within
	 * session_cipher_decrypt_pre_key_signal_message() and
	 * session_cipher_decrypt_signal_message() after decryption is complete
	 * but before the updated session state has been committed to the session
	 * store. If the callback function returns a negative value, then the
	 * decrypt function will immediately fail with an error.
	 *
	 * This a callback allows some implementations to store the committed plaintext
	 * to their local message store first, in case they are concerned with a crash
	 * or write error happening between the time the session state is updated but
	 * before they're able to successfully store the plaintext to disk.
	 *
	 * @param callback the callback function to set
	 * @param user_data user data pointer provided to the callback
	 */
	pub fn session_cipher_set_decryption_callback(
			cipher:   *mut Cipher,
			callback: Option<unsafe extern "C" fn(
				cipher:    *mut Cipher,
				plaintext: *mut ::Buffer,
				decrypt_context: *mut types::c_void
			) -> ::OkStatus>
		);
	
	/**
	 * Encrypt a message.
	 *
	 * @param padded_message The plaintext message bytes, optionally padded to a constant multiple.
	 * @param padded_message_len The length of the data pointed to by padded_message
	 * @param encrypted_message Set to a ciphertext message encrypted to the recipient+device tuple.
	 *
	 * @return AX_SUCCESS on success, negative on error
	 */
	pub fn session_cipher_encrypt(
			cipher: *mut Cipher,
			padded_message:    *const u8, padded_message_len: usize,
			encrypted_message: *mut *mut protocol::CiphertextMessage
		) -> ::OkStatus;
	
	/**
	 * Decrypt a message.
	 *
	 * @param ciphertext The PreKeySignalMessage to decrypt.
	 * @param decrypt_context Optional context pointer associated with the
	 *   ciphertext, which is passed to the decryption callback function
	 * @param plaintext Set to a newly allocated buffer containing the plaintext.
	 *
	 * @retval AX_SUCCESS Success
	 * @retval AX_ERR_INVALID_MESSAGE if the input is not valid ciphertext.
	 * @retval AX_ERR_DUPLICATE_MESSAGE if the input is a message that has already been received.
	 * @retval AX_ERR_LEGACY_MESSAGE if the input is a message formatted by a protocol version that
	 *                               is no longer supported.
	 * @retval AX_ERR_INVALID_KEY_ID when there is no local pre_key_record
	 *                               that corresponds to the pre key ID in the message.
	 * @retval AX_ERR_INVALID_KEY when the message is formatted incorrectly.
	 * @retval AX_ERR_UNTRUSTED_IDENTITY when the identity key of the sender is untrusted.
	 */
	pub fn session_cipher_decrypt_pre_key_signal_message(
			cipher:          *mut Cipher,
			ciphertext:      *mut protocol::PreKeySignalMessage,
			decrypt_context: *mut types::c_void,
			plaintext:       *mut *mut ::Buffer
		) -> ::OkStatus;
	
	/**
	 * Decrypt a message.
	 *
	 * @param ciphertext The SignalMessage to decrypt.
	 * @param decrypt_context Optional context pointer associated with the
	 *   ciphertext, which is passed to the decryption callback function
	 * @param plaintext Set to a newly allocated buffer containing the plaintext.
	 *
	 * @retval AX_SUCCESS Success
	 * @retval AX_ERR_INVALID_MESSAGE if the input is not valid ciphertext.
	 * @retval AX_ERR_DUPLICATE_MESSAGE if the input is a message that has already been received.
	 * @retval AX_ERR_LEGACY_MESSAGE if the input is a message formatted by a protocol version that
	 *                               is no longer supported.
	 * @retval AX_ERR_NO_SESSION if there is no established session for this contact.
	 */
	pub fn session_cipher_decrypt_signal_message(
			cipher:     *mut Cipher,
			ciphertext: *mut protocol::SignalMessage,
			decrypt_context: *mut types::c_void,
			plaintext:       *mut *mut ::Buffer
		) -> ::OkStatus;
	
	/**
	 * Gets the remote registration ID for this session cipher.
	 *
	 * @param remote_id Set to the value of the remote registration ID
	 *
	 * @return AX_SUCCESS on success, negative on error
	 */
	pub fn session_cipher_get_remote_registration_id(
			cipher: *mut Cipher,
			remote_id: *mut u32
		) -> ::OkStatus;
	
	/**
	 * Gets the version of the session associated with this session cipher.
	 *
	 * @param version Set to the value of the session version
	 *
	 * @retval AX_SUCCESS Success
	 * @retval AX_ERR_NO_SESSION if no session could be found
	 */
	pub fn session_cipher_get_session_version(
			cipher: *mut Cipher,
			version: *mut u32
		) -> ::OkStatus;
	
	
	pub fn session_cipher_free(
			cipher: *mut Cipher
		);
}


// session_pre_key.h //
pub static PRE_KEY_MEDIUM_MAX_VALUE: u32 = 0xFFFFFF;

extern "C" {
	pub fn session_pre_key_create(
			pre_key: *mut *mut PreKey,
			id: u32,
			key_pair: *mut curve::KeyPair
		) -> ::OkStatus;
	
	pub fn session_pre_key_serialize(
			buffer: *mut *mut ::Buffer,
			pre_key: *const PreKey
		) -> ::OkStatus;
	
	pub fn session_pre_key_deserialize(
			pre_key: *mut *mut PreKey,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn session_pre_key_get_id(
			pre_key: *const PreKey
		) -> u32;
	
	pub fn session_pre_key_get_key_pair(
			pre_key: *const PreKey
		) -> *mut curve::KeyPair;
	
	pub fn session_pre_key_destroy(
			pre_key: *mut PreKey
		);
	
	
	
	pub fn session_signed_pre_key_create(
			pre_key: *mut *mut SignedPreKey,
			id: u32,
			timestamp: u64,
			key_pair: *mut curve::KeyPair,
			signature: *const u8, signature_len: usize
		) -> ::OkStatus;
	
	pub fn session_signed_pre_key_serialize(
			buffer:  *mut *mut ::Buffer,
			pre_key: *const SignedPreKey
		) -> ::OkStatus;
	
	pub fn session_signed_pre_key_deserialize(
			pre_key: *mut *mut SignedPreKey,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn session_signed_pre_key_get_id(
			pre_key: *const SignedPreKey
		) -> u32;
	
	pub fn session_signed_pre_key_get_timestamp(
			pre_key: *const SignedPreKey
		) -> u64;
	pub fn session_signed_pre_key_get_key_pair(
			pre_key: *const SignedPreKey
		) -> *mut curve::KeyPair;
	
	pub fn session_signed_pre_key_get_signature(
			pre_key: *const SignedPreKey
		) -> *const u8;
	
	pub fn session_signed_pre_key_get_signature_len(
			pre_key: *const SignedPreKey
		) -> usize;
	
	pub fn session_signed_pre_key_destroy(
			pre_key: *mut SignedPreKey
		);
	
	
	
	pub fn session_pre_key_bundle_create(
			bundle: *mut *mut PreKeyBundle,
			registration_id: u32,
			device_id: types::c_int,
			pre_key_id: u32,
			pre_key_public: *mut curve::PublicKey,
			signed_pre_key_id: u32,
			signed_pre_key_public: *mut curve::PublicKey,
			signed_pre_key_signature_data: *const u8, signed_pre_key_signature_len: usize,
			identity_key: *mut curve::PublicKey
		) -> ::OkStatus;
	
	pub fn session_pre_key_bundle_get_registration_id(
			bundle: *const PreKeyBundle
		) -> u32;
	
	pub fn session_pre_key_bundle_get_device_id(
			bundle: *const PreKeyBundle
		) -> types::c_int;
	
	pub fn session_pre_key_bundle_get_pre_key_id(
			bundle: *const PreKeyBundle
		) -> u32;
	
	pub fn session_pre_key_bundle_get_pre_key(
			bundle: *const PreKeyBundle
		) -> *mut curve::PublicKey;
	
	pub fn session_pre_key_bundle_get_signed_pre_key_id(
			bundle: *const PreKeyBundle
		) -> u32;
	
	pub fn session_pre_key_bundle_get_signed_pre_key(
			bundle: *const PreKeyBundle
		) -> *mut curve::PublicKey;
	
	pub fn session_pre_key_bundle_get_signed_pre_key_signature(
			bundle: *const PreKeyBundle
		) -> *mut ::Buffer;
	
	pub fn session_pre_key_bundle_get_identity_key(
			bundle: *const PreKeyBundle
		) -> *mut curve::PublicKey;
	
	pub fn session_pre_key_bundle_destroy(
			bundle: *mut PreKeyBundle
		);
}


// session_record.h //
extern "C" {
	pub fn session_record_create(
			record: *mut *mut Record,
			state: *mut State,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn session_record_serialize(
			buffer: *mut *mut ::Buffer,
			record: *const Record
		) -> ::OkStatus;
	
	pub fn session_record_deserialize(
			record: *mut *mut Record,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn session_record_copy(
			record: *mut *mut Record,
			other_record: *mut Record,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	
	pub fn session_record_has_session_state(
			record: *mut Record,
			version: u32,
			alice_base_key:	*const curve::PublicKey
		) -> ::Response;
	
	pub fn session_record_get_state(
			record: *mut Record
		) -> *mut State;
	
	pub fn session_record_set_state(
			record: *mut Record,
			state: *mut State
		);
	
	
	
	pub fn session_record_get_previous_states_head(
			record: *const Record
		) -> *mut RecordStateNode;
	
	pub fn session_record_get_previous_states_element(
			node: *const RecordStateNode
		) -> *mut State;
	
	pub fn session_record_get_previous_states_next(
			node: *const RecordStateNode
		) -> *mut RecordStateNode;
	
	/**
	 * Removes the specified node in the previous states list.
	 * @param node the node to remove
	 * @return the node immediately following the removed node, or null if at the end of the list
	 */
	pub fn session_record_get_previous_states_remove(
			record: *mut Record,
			node:   *mut RecordStateNode
		) -> *mut RecordStateNode;
	
	
	
	pub fn session_record_is_fresh(
			record: *mut Record
		) -> ::Response;
	
	/**
	 * Move the current session_state into the list of "previous" session states,
	 * and replace the current session_state with a fresh reset instance.
	 *
	 * @return 0 on success, negative on failure
	 */
	pub fn session_record_archive_current_state(
			record: *mut Record
		) -> ::OkStatus;
	
	pub fn session_record_promote_state(
			record: *mut Record,
			promoted_state: *mut State
		) -> ::OkStatus;
	
	pub fn session_record_destroy(
			record: *mut Record
		);
}


// session_state.h //
extern "C" {
	pub fn session_state_create(
			state: *mut *mut State,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn session_state_serialize(
			buffer: *mut *mut ::Buffer,
			state: *mut State
		) -> ::OkStatus;
	
	pub fn session_state_deserialize(
			state: *mut *mut State,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn session_state_copy(
			state: *mut *mut State,
			other_state: *mut State,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	pub fn session_state_set_session_version(
			state: *mut State,
			version: u32
		);
	
	pub fn session_state_get_session_version(
			state: *const State
		) -> u32;
	
	
	pub fn session_state_set_local_identity_key(
			state: *mut State,
			identity_key: *mut curve::PublicKey
		);
	
	pub fn session_state_get_local_identity_key(
			state: *const State
		) -> *mut curve::PublicKey;
	
	
	pub fn session_state_set_remote_identity_key(
			state: *mut State,
			identity_key: *mut curve::PublicKey
		);
	
	pub fn session_state_get_remote_identity_key(
			state: *const State
		) -> *mut curve::PublicKey;
	
	
	pub fn session_state_set_root_key(
			state: *mut State,
			root_key: *mut ratchet::RootKey
		);
	
	pub fn session_state_get_root_key(
			state: *const State
		) -> *mut ratchet::RootKey;
	
	
	pub fn session_state_set_previous_counter(
			state: *mut State, counter: u32
		);
	
	pub fn session_state_get_previous_counter(
			state: *const State
		) -> u32;
	
	
	pub fn session_state_set_sender_chain(
			state: *mut State,
			sender_ratchet_key_pair: *mut curve::KeyPair,
			chain_key: *mut ratchet::ChainKey
		);
	
	pub fn session_state_get_sender_ratchet_key(
			state: *const State
		) -> *mut curve::PublicKey;
	
	pub fn session_state_get_sender_ratchet_key_pair(
			state: *const State
		) -> *mut curve::KeyPair;
	
	pub fn session_state_get_sender_chain_key(
			state: *const State
		) -> *mut ratchet::ChainKey;
	
	pub fn session_state_set_sender_chain_key(
			state: *mut State,
			chain_key:  *mut ratchet::ChainKey
		) -> ::OkStatus;
	
	pub fn session_state_has_sender_chain(
			state: *const State
		) -> ::Response;
	
	
	pub fn session_state_has_message_keys(
			state: *mut State,
			sender_ephemeral: *mut curve::PublicKey,
			counter: u32
		) -> ::Response;
	
	pub fn session_state_remove_message_keys(
			state: *mut State,
			message_keys_result: *mut ratchet::MessageKeys,
			sender_ephemeral:    *mut curve::PublicKey,
			counter: u32
		) -> ::OkStatus;
	
	pub fn session_state_set_message_keys(
			state: *mut State,
			sender_ephemeral: *mut curve::PublicKey,
			message_keys: *mut ratchet::MessageKeys
		) -> ::OkStatus;
	
	
	pub fn session_state_add_receiver_chain(
			state: *mut State,
			sender_ratchet_key: *mut curve::PublicKey,
			chain_key: *mut ratchet::ChainKey
		) -> ::OkStatus;
	
	pub fn session_state_set_receiver_chain_key(
			state: *mut State,
			sender_ephemeral: *mut curve::PublicKey,
			chain_key:        *mut ratchet::ChainKey
		) -> ::OkStatus;
	
	pub fn session_state_get_receiver_chain_key(
			state: *mut State,
			sender_ephemeral: *mut curve::PublicKey
		) -> *mut ratchet::ChainKey;
	
	
	pub fn session_state_set_pending_key_exchange(
			state: *mut State,
			sequence: u32,
			our_base_key:     *mut curve::KeyPair,
			our_ratchet_key:  *mut curve::KeyPair,
			our_identity_key: *mut ratchet::IdentityKeyPair
		);
	
	pub fn session_state_get_pending_key_exchange_sequence(
			state: *mut State
		) -> u32;
	
	pub fn session_state_get_pending_key_exchange_base_key(
			state: *const State
		) -> *mut curve::KeyPair;
	
	pub fn session_state_get_pending_key_exchange_ratchet_key(
			state: *const State
		) -> *mut curve::KeyPair;
	
	pub fn session_state_get_pending_key_exchange_identity_key(
			state: *const State
		) -> *mut ratchet::IdentityKeyPair;
	
	pub fn session_state_has_pending_key_exchange(
			state: *const State
		) -> ::Response;
	
	
	pub fn session_state_set_unacknowledged_pre_key_message(
			state:      *mut State,
			pre_key_id:	*const u32,
			signed_pre_key_id: u32,
			base_key: *mut curve::PublicKey
		);
	
	pub fn session_state_unacknowledged_pre_key_message_has_pre_key_id(
			state: *const State
		) -> ::Response;
	
	pub fn session_state_unacknowledged_pre_key_message_get_pre_key_id(
			state: *const State
		) -> u32;
	
	pub fn session_state_unacknowledged_pre_key_message_get_signed_pre_key_id(
			state: *const State
		) -> u32;
	
	pub fn session_state_unacknowledged_pre_key_message_get_base_key(
			state: *const State
		) -> *mut curve::PublicKey;
	
	pub fn session_state_has_unacknowledged_pre_key_message(
			state: *const State
		) -> ::Response;
	
	pub fn session_state_clear_unacknowledged_pre_key_message(
			state: *mut State
		);
	
	
	pub fn session_state_set_remote_registration_id(
			state: *mut State,
			id: u32
		);
	
	pub fn session_state_get_remote_registration_id(
			state: *const State
		) -> u32;
	
	
	pub fn session_state_set_local_registration_id(
			state: *mut State,
			id: u32
		);
	
	pub fn session_state_get_local_registration_id(
			state: *const State
		) -> u32;
	
	
	pub fn session_state_set_needs_refresh(
			state: *mut State,
			value: ::Response
		);
	
	pub fn session_state_get_needs_refresh(
			state: *const State
		) -> ::Response;
	
	
	pub fn session_state_set_alice_base_key(
			state: *mut State,
			key: *mut curve::PublicKey
		);
	
	pub fn session_state_get_alice_base_key(
			state: *const State
		) -> *mut curve::PublicKey;
	
	
	pub fn session_state_destroy(
			state: *mut State
		);
}