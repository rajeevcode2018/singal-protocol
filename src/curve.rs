// Curve key types
pub enum PublicKey  {}
pub enum PrivateKey {}
pub enum KeyPair    {}

extern "C" {
	pub fn curve_decode_point(
			public_key: *mut *mut PublicKey,
			key_data: *const u8, key_len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn ec_public_key_compare(
			key1: *const PublicKey,
			key2: *const PublicKey
		) -> ::Comparision;
	
	pub fn ec_public_key_memcmp(
			key1: *const PublicKey,
			key2: *const PublicKey
		) -> ::Comparision;
	
	
	
	/**
	 * Serialize the public key into a buffer that can be stored.
	 * The format of this data is compatible with the input format of
	 * curve_decode_point().
	 *
	 * @param buffer Pointer to a buffer that will be allocated by this function
	 *     and filled with the contents of the key. The caller is responsible for
	 *     freeing this buffer with axolotl_buffer_free().
	 * @param key Key to serialize
	 * @return 0 on success, negative on failure
	 */
	pub fn ec_public_key_serialize(
			buffer: *mut *mut ::Buffer,
			key:    *const PublicKey
		) -> ::OkStatus;
	
	
	pub fn ec_public_key_destroy(
			key: *mut PublicKey
		);
	
	
	
	pub fn curve_decode_private_point(
			private_key: *mut *mut PrivateKey,
			key_data:  *const u8, key_len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn ec_private_key_compare(
			key1: *const PrivateKey,
			key2: *const PrivateKey
		) -> ::Comparision;
	
	
	
	/**
	 * Serialize the private key into a buffer that can be stored.
	 * The format of this data is compatible with the input format of
	 * curve_decode_private_point().
	 *
	 * @param buffer Pointer to a buffer that will be allocated by this function
	 *     and filled with the contents of the key. The caller is responsible for
	 *     freeing this buffer with axolotl_buffer_free().
	 * @param key Key to serialize
	 * @return 0 on success, negative on failure
	 */
	pub fn ec_private_key_serialize(
			buffer: *mut *mut ::Buffer,
			key: *const PrivateKey
		) -> ::OkStatus;
	
	
	
	pub fn ec_private_key_destroy(
			key: *mut PrivateKey
		);
	
	
	
	pub fn ec_key_pair_create(
			key_pair:    *mut *mut KeyPair,
			public_key:  *mut PublicKey,
			private_key: *mut PrivateKey
		) -> ::OkStatus;
	
	pub fn ec_key_pair_get_public(
			key_pair: *const KeyPair
		) -> *mut PublicKey;
	
	pub fn ec_key_pair_get_private(
			key_pair: *const KeyPair
		) -> *mut PublicKey;
	
	pub fn ec_key_pair_destroy(
			key_pair: *mut KeyPair
		);
	
	
	
	pub fn curve_generate_private_key(
			context:     *const ::Context,
			private_key: *mut *mut PrivateKey
		) -> ::OkStatus;
	
	pub fn curve_generate_public_key(
			public_key:  *mut *mut PublicKey,
			private_key: *const PrivateKey
		) -> ::OkStatus;
	
	pub fn curve_generate_key_pair(
			context: *mut ::Context,
			key_pair: *mut *mut KeyPair
		) -> ::OkStatus;
	
	
	
	pub fn curve_calculate_agreement(
			shared_key_data: *mut *mut u8,
			public_key:  *const PublicKey,
			private_key: *const PrivateKey
		) -> ::ValueStatus;
	
	pub fn curve_verify_signature(
			signing_key:    *const PublicKey,
			message_data:   *const u8, message_len:   usize,
			signature_data: *const u8, signature_len: usize
		) -> ::ResponseStatus;
	
	pub fn curve_calculate_signature(
			context: *const ::Context,
			signature:   *mut *mut ::Buffer,
			signing_key: *const PrivateKey,
			message_data: *const u8, message_len: usize
		) -> ::OkStatus;
}