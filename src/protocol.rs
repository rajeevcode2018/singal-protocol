use curve;


// Protocol types
pub enum KeyExchangeMessage           {}
pub enum CiphertextMessage            {}
pub enum SignalMessage                {}
pub enum PreKeySignalMessage          {}
pub enum SenderKeyMessage             {}
pub enum SenderKeyDistributionMessage {}


// protocol.h //

bitflags! {
	#[repr(C)]
	pub flags KeyExchangeFlags: u32 {
		const KEY_EXCHANGE_INITIATE              = 0x01,
		const KEY_EXCHANGE_RESPONSE              = 0x02,
		const KEY_EXCHANGE_SIMULTAENOUS_INITIATE = 0x04,
	}
}

pub mod ciphertext {
	pub static UNSUPPORTED_VERSION: u8 = 1;
	pub static CURRENT_VERSION:     u8 = 3;

	#[repr(i32)]
	pub enum Type {
		Signal       = 2,
		PreKey       = 3,
		SenderKey    = 4,
		Distribution = 5,
	}

	/// Worst case overhead. Not always accurate, but good enough for padding.
	pub static ENCRYPTED_MESSAGE_OVERHEAD: u8 = 53;
}

extern "C" {
	pub fn key_exchange_message_create(
			message: *mut *mut KeyExchangeMessage,
			message_version: u8,
			sequence: u32, flags: KeyExchangeFlags,
			base_key: *mut curve::PublicKey,
			base_key_signature: *mut u8,
			ratchet_key:  *mut curve::PublicKey,
			identity_key: *mut curve::PublicKey
		) -> ::OkStatus;
	
	
	
	pub fn key_exchange_message_deserialize(
			message: *mut *mut KeyExchangeMessage,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	
	
	pub fn key_exchange_message_get_serialized(
			message: *const KeyExchangeMessage
		) -> *mut ::Buffer;
	
	
	
	pub fn key_exchange_message_get_version(
			message: *const KeyExchangeMessage
		) -> u8;
	
	pub fn key_exchange_message_get_base_key(
			message: *const KeyExchangeMessage
		) -> *mut curve::PublicKey;
	
	pub fn key_exchange_message_get_base_key_signature(
			message: *mut KeyExchangeMessage
		) -> *mut u8;
	
	pub fn key_exchange_message_get_ratchet_key(
			message: *mut KeyExchangeMessage
		) -> *mut curve::PublicKey;
	
	pub fn key_exchange_message_get_identity_key(
			message: *mut KeyExchangeMessage
		) -> *mut curve::PublicKey;
	
	pub fn key_exchange_message_has_identity_key(
			message: *mut KeyExchangeMessage
		) -> ::Response;
	
	pub fn key_exchange_message_get_max_version(
			message: *mut KeyExchangeMessage
		) -> u8;
	
	pub fn key_exchange_message_is_response(
			message: *mut KeyExchangeMessage
		) -> ::Response;
	
	pub fn key_exchange_message_is_initiate(
			message: *mut KeyExchangeMessage
		) -> ::Response;
	
	pub fn key_exchange_message_is_response_for_simultaneous_initiate(
			message: *mut KeyExchangeMessage
		) -> ::Response;
	
	pub fn key_exchange_message_get_flags(
			message: *mut KeyExchangeMessage
		) -> KeyExchangeFlags;
	
	pub fn key_exchange_message_get_sequence(
			message: *mut KeyExchangeMessage
		) -> u32;
	
	pub fn key_exchange_message_destroy(
			message: *mut KeyExchangeMessage
		);
	
	
	
	pub fn ciphertext_message_get_type(
			message: *const CiphertextMessage
		) -> ciphertext::Type;
	
	pub fn ciphertext_message_get_serialized(
			message: *const CiphertextMessage
		) -> *mut ::Buffer;
	
	
	
	pub fn signal_message_create(
			message: *mut *mut SignalMessage,
			message_version: u8,
			mac_key: *const u8, mac_key_len: usize,
			sender_ratchet_key: *mut curve::PublicKey,
			counter: u32, previous_counter: u32,
			ciphertext: *const u8, ciphertext_len: usize,
			sender_identity_key:   *mut curve::PublicKey,
			receiver_identity_key: *mut curve::PublicKey,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn signal_message_deserialize(
			message: *mut *mut SignalMessage,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn signal_message_copy(
			message: *mut *mut SignalMessage,
			other_message: *mut SignalMessage,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn signal_message_get_sender_ratchet_key(
			message: *const SignalMessage
		) -> *mut curve::PublicKey;
	
	pub fn signal_message_get_message_version(
			message: *const SignalMessage
		) -> u8;
	
	pub fn signal_message_get_counter(
			message: *const SignalMessage
		) -> u32;
	
	pub fn signal_message_get_body(
			message: *const SignalMessage
		) -> *mut ::Buffer;
	
	/**
	 * Verify the MAC on the Signal message.
	 *
	 * @return 1 if verified, 0 if invalid, negative on error
	 */
	pub fn signal_message_verify_mac(
			message: *mut SignalMessage,
			message_version: u8,
			sender_identity_key:   *mut curve::PublicKey,
			receiver_identity_key: *mut curve::PublicKey,
			mac_key: *const u8, mac_key_len: usize,
			global_context: *mut ::Context
		) -> ::ResponseStatus;
	
	pub fn signal_message_is_legacy(
			data: *const u8, len: usize
		) -> ::Response;
	
	pub fn signal_message_destroy(
			message: *mut SignalMessage
		);
	
	
	
	pub fn pre_key_signal_message_create(
			pre_key_message: *mut *mut PreKeySignalMessage,
			message_version: u8,
			registration_id: u32,
			pre_key_id: *const u32,
			signed_pre_key_id: u32,
			base_key:     *mut curve::PublicKey,
			identity_key: *mut curve::PublicKey,
			message:        *mut SignalMessage,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn pre_key_signal_message_deserialize(
			message: *mut *mut PreKeySignalMessage,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn pre_key_signal_message_copy(
			message: *mut *mut PreKeySignalMessage,
			other_message: *mut PreKeySignalMessage,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn pre_key_signal_message_get_message_version(
			message: *const PreKeySignalMessage
		) -> u8;
	
	pub fn pre_key_signal_message_get_identity_key(
			message: *const PreKeySignalMessage
		) -> *mut curve::PublicKey;
	
	pub fn pre_key_signal_message_get_registration_id(
			message: *const PreKeySignalMessage
		) -> u32;
	
	pub fn pre_key_signal_message_has_pre_key_id(
			message: *const PreKeySignalMessage
		) -> ::Response;
	
	pub fn pre_key_signal_message_get_pre_key_id(
			message: *const PreKeySignalMessage
		) -> u32;
	
	pub fn pre_key_signal_message_get_signed_pre_key_id(
			message: *const PreKeySignalMessage
		) -> u32;
	
	pub fn pre_key_signal_message_get_base_key(
			message: *const PreKeySignalMessage
		) -> *mut curve::PublicKey;
	
	pub fn pre_key_signal_message_get_signal_message(
			message: *const PreKeySignalMessage
		) -> *mut SignalMessage;
	
	pub fn pre_key_signal_message_destroy(
			message: *mut PreKeySignalMessage
		);
	
	
	
	pub fn sender_key_message_create(
			message: *mut *mut SenderKeyMessage,
			key_id:    u32,
			iteration: u32,
			ciphertext: *const u8, ciphertext_len: usize,
			signature_key:  *mut curve::PrivateKey,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_message_deserialize(
			message: *mut *mut SenderKeyMessage,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_message_copy(
			message: *mut *mut SenderKeyMessage,
			other_message:  *mut SenderKeyMessage,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_message_get_key_id(
			message: *mut SenderKeyMessage
		) -> u32;
	
	pub fn sender_key_message_get_iteration(
			message: *mut SenderKeyMessage
		) -> u32;
	
	pub fn sender_key_message_get_ciphertext(
			message: *mut SenderKeyMessage
		) -> *mut ::Buffer;
	
	pub fn sender_key_message_verify_signature(
			message: *mut SenderKeyMessage,
			signature_key: *mut curve::PublicKey
		) -> ::OkStatus;
	
	pub fn sender_key_message_destroy(
			message: *mut SenderKeyMessage
		);
	
	
	
	pub fn sender_key_distribution_message_create(
			message: *mut *mut SenderKeyDistributionMessage,
			id:        u32,
			iteration: u32,
			chain_key: *const u8, chain_key_len: usize,
			signature_key:  *mut curve::PublicKey,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_distribution_message_deserialize(
			message: *mut *mut SenderKeyDistributionMessage,
			data: *const u8, len: usize,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_distribution_message_copy(
			message: *mut *mut SenderKeyDistributionMessage,
			other_message: *mut SenderKeyDistributionMessage,
			global_context: *mut ::Context
		) -> ::OkStatus;
	
	pub fn sender_key_distribution_message_get_id(
			message: *mut SenderKeyDistributionMessage
		) -> u32;
	
	pub fn sender_key_distribution_message_get_iteration(
			message: *mut SenderKeyDistributionMessage
		) -> u32;
	
	pub fn sender_key_distribution_message_get_chain_key(
			message: *mut SenderKeyDistributionMessage
		) -> *mut ::Buffer;
	
	pub fn sender_key_distribution_message_get_signature_key(
			message: *mut SenderKeyDistributionMessage
		) -> *mut curve::PublicKey;
	
	pub fn sender_key_distribution_message_destroy(
			message: *mut SenderKeyDistributionMessage
		);
}